# Flask Random Facts Generator

Using Flask App for updated with facts 

### requirements module :)

1. [pip3 install randfacts](https://pypi.org/project/randfacts/)
2. [pip3 install Flask](https://pypi.org/project/Flask/)

### randfacts:
> Randfacts is a python library that generates random facts. You can use randfacts.get_fact() to return a random fun fact. Disclaimer: Facts are not guaranteed to be true.

### Flask:
>Flask is a web framework, it’s a Python module that lets you develop web applications easily. It’s has a small and easy-to-extend core: it’s a microframework that doesn’t include an ORM (Object Relational Manager) or such features.

It does have many cool features like url routing, template engine. It is a WSGI web app framework.

### Run Applications :)
![alt text](https://xp.io/storage/1BKVP5De.png)
### How it’s work :
> Every time you click To generate Button a post method
get active and return a Facts from randfacts module to Html
